package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable SessionDTO session) throws Exception;


}
