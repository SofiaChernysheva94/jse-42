package ru.t1.chernysheva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {


    @Insert("INSERT INTO tm_test.tm_user (id, login, email, " +
            "first_name, last_name, middle_name, role, locked ,password_hash)" +
            " VALUES (#{user.id}, #{user.login}, #{user.email},  " +
            "#{user.firstName}, #{user.lastName}, #{user.middleName}, #{user.role}, #{user.locked} ,#{user.passwordHash})")
    void add(@NotNull @Param("user") UserDTO user);

    @Delete("DELETE FROM tm_test.tm_user CASCADE")
    void clear();

    @Select("SELECT * FROM tm_test.tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable List<UserDTO> findAll();

    @Select("SELECT * FROM tm_test.tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_test.tm_user WHERE LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_test.tm_user")
    int getSize();

    @Delete("DELETE FROM tm_test.tm_user WHERE id = #{id}")
    void remove(@NotNull UserDTO user);

    @Update("UPDATE tm_test.tm_user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, " +
            "locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, role = #{role} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Select("SELECT * FROM tm_test.tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm_test.tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByEmail(@NotNull @Param("email") String email);


}
