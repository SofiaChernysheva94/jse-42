package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindFromProjectResponse extends AbstractTaskResponse {

    public TaskUnbindFromProjectResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
