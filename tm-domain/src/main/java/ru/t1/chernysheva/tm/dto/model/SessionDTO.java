package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_session", schema = "tm_test")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Basic
    @Column(name = "userId")
    private String userId;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

}
