package ru.t1.chernysheva.tm.exception.system;

import ru.t1.chernysheva.tm.exception.system.AbstractSystemException;

public class AuthenticationException extends AbstractSystemException {

    public AuthenticationException () {

        super("Error! Incorrect login or password entered. Please try again...");
    }

}
